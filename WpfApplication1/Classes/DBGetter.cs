﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Windows.Forms;
using WpfApplication1.Classes.DatabaseClasses.Characters;

namespace WpfApplication1
{
    class DBGetter
    {
        OleDbConnection connection;
        public List<CharacterInstance> xD = new List<CharacterInstance>();
        public DBGetter()
        {
            var DBPath = AppDomain.CurrentDomain.BaseDirectory + "\\BazaDone.mdb";

            connection = new OleDbConnection("Provider=Microsoft.Jet.OleDb.4.0;" + "Data Source=" + DBPath);
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }   
        }
        public void selectAll()
        {
            List<Attr> attributes = new List<Attr>();
            List<Skill> skills = new List<Skill>();
            List<Perk> perks = new List<Perk>();    
            Class magicClass ;
            Race race;
            CharacterDefinition def;
            string readedName= null;
            try
            {
                #region Base SELECT
                OleDbCommand selectBase = new OleDbCommand("SELECT DefinicjaPostac.Imie,DefinicjaPostac.ID_Rasa,Tytul.Nazwa,Atrybut.Wartosc,MocWyczyn.Nazwa,Atut.Nazwa,DefinicjaPostac.MaxPW,"
                + " Klasa.Nazwa,Rasa.Nazwa,DefinicjaPostac.Ranga,DefinicjaPostac.Poziom"
                + " FROM ((((((((((((((((((((((((( DefinicjaPostac "
                + " INNER JOIN Rasa "
                + " ON Rasa.ID_Rasa = DefinicjaPostac.ID_Rasa) "
                + " INNER JOIN Rozmiar "
                + " ON Rozmiar.ID_Rozmiar = Rasa.ID_Rozmiar) "
                + " INNER JOIN Klasa "
                + " ON Klasa.ID_Klasa = DefinicjaPostac.ID_Klasa) "

                + " INNER JOIN RelacjaAtutDefinicjaPostaci "
                + " ON RelacjaAtutDefinicjaPostaci.ID_DefinicjaPostaci = DefinicjaPostac.ID_DefinicjaPostac) "
                + " INNER JOIN Atut "
                + " ON RelacjaAtutDefinicjaPostaci.ID_Atut = Atut.ID_Atut) "

                + " INNER JOIN RelacjaAtrybutDefinicjaPostaci "
                + " ON RelacjaAtrybutDefinicjaPostaci.ID_DefinicjaPostaci = DefinicjaPostac.ID_DefinicjaPostac) "
                + " INNER JOIN Atrybut "
                + " ON RelacjaAtrybutDefinicjaPostaci.ID_Atrybut = Atrybut.ID_Atrybut) "
                + " INNER JOIN Tytul "
                + " ON Tytul.ID_Tytul = Atrybut.ID_Tytul) "

                + " INNER JOIN BiegloscWZbrojach "
                + " ON BiegloscWZbrojach.ID_BiegloscWZbrojach = Klasa.ID_BiegloscWZbrojach) "
                + " INNER JOIN BiegloscWBroniach "
                + " ON BiegloscWBroniach.ID_BiegloscWBroniach = Klasa.ID_BiegloscWBroniach) "

                + " INNER JOIN RelacjaDefinicjaPostacMocWyczyn "
                + " ON RelacjaDefinicjaPostacMocWyczyn.ID_DefinicjaPostac = DefinicjaPostac.ID_DefinicjaPostac) "
                + " INNER JOIN MocWyczyn "
                + " ON RelacjaDefinicjaPostacMocWyczyn.ID_MocWyczyn = MocWyczyn.ID_MocWyczyn) "

                + " INNER JOIN RelacjaMocWyczynSlowoKluczowe "
                + " ON RelacjaMocWyczynSlowoKluczowe.ID_MocWyczyn = MocWyczyn.ID_MocWyczyn) "
                + " INNER JOIN SlowoKluczowe "
                + " ON RelacjaMocWyczynSlowoKluczowe.ID_SlowoKluczowe = SlowoKluczowe.ID_SlowoKluczowe) "

                + " INNER JOIN Efekt "
                + " ON Efekt.ID_Efekt = SlowoKluczowe.ID_Efekt) "


                + " INNER JOIN RelacjaKlasaMocWyczyn "
                + " ON RelacjaKlasaMocWyczyn.ID_Klasa = Klasa.ID_Klasa) "
                + " INNER JOIN MocWyczyn AS MocWyczynKlasy "
                + " ON RelacjaKlasaMocWyczyn.ID_MocWyczyn = MocWyczynKlasy.ID_MocWyczyn) "

                + " INNER JOIN RelacjaMocWyczynSlowoKluczowe AS RelacjaMocWyczynKlasa "
                + " ON RelacjaMocWyczynKlasa.ID_MocWyczyn = MocWyczynKlasy.ID_MocWyczyn) "
                + " INNER JOIN SlowoKluczowe AS SlowoKlasy "
                + " ON RelacjaMocWyczynKlasa.ID_SlowoKluczowe = SlowoKlasy.ID_SlowoKluczowe) "

                + " INNER JOIN Efekt AS EfektKlasy "
                + " ON EfektKlasy.ID_Efekt = SlowoKluczowe.ID_Efekt) "

                + " INNER JOIN RelacjaRasaMocWyczyn "
                + " ON RelacjaRasaMocWyczyn.ID_Rasa = Rasa.ID_Rasa) "
                + " INNER JOIN MocWyczyn AS MocWyczynRasy "
                + " ON RelacjaRasaMocWyczyn.ID_MocWyczyn = MocWyczynRasy.ID_MocWyczyn) "

                + " INNER JOIN RelacjaMocWyczynSlowoKluczowe AS RelacjaMocWyczynRasa "
                + " ON RelacjaMocWyczynRasa.ID_MocWyczyn = MocWyczynRasy.ID_MocWyczyn) "
                + " INNER JOIN SlowoKluczowe AS SlowoRasy "
                + " ON RelacjaMocWyczynRasa.ID_SlowoKluczowe = SlowoRasy.ID_SlowoKluczowe) "

                + " INNER JOIN Efekt AS EfektRasy "
                + " ON EfektRasy.ID_Efekt = SlowoRasy.ID_Efekt) "
                , connection);

                OleDbDataReader readerBase = selectBase.ExecuteReader();
                while (readerBase.Read())
                {
                    Console.Write("21");
                    var Name = readerBase.GetString(0);
                        if (readedName != Name)
                    {
                        readedName = Name;
                        attributes = new List<Attr>();
                        skills = new List<Skill>();
                        perks = new List<Perk>();
                        var ID_Race = readerBase.GetInt32(1);
                        attributes.Add(new Attr(readerBase.GetString(2), readerBase.GetInt32(3)));
                        string[] tag = { "tag", "niet" };
                        skills.Add(new Skill(null, 0, readerBase.GetString(4), null, 0, null,tag));
                        perks.Add(new Perk(readerBase.GetString(5), null, null));
                        var Health = readerBase.GetInt32(6);
                        var ClassName = readerBase.GetString(7);
                        var RaceName = readerBase.GetString(8);
                        var rank = readerBase.GetString(9);
                        var level = readerBase.GetInt32(10);
                        magicClass = new Class(new List<Classes.DatabaseClasses.Bonuses.AttributeBonus>(), new List<Classes.DatabaseClasses.Bonuses.AbilityBonus>()
                            , new List<Classes.DatabaseClasses.Bonuses.DefenceBonus>(), new List<Classes.DatabaseClasses.Bonuses.TestBonus>()
                            , new List<KeyValuePair<string, string>>(), new List<KeyValuePair<string, string>>(), new List<Skill>(), ClassName);
                        race = new Race(new List<Classes.DatabaseClasses.Bonuses.AttributeBonus>(),new List<Classes.DatabaseClasses.Bonuses.AbilityBonus>()
                            , new List<Classes.DatabaseClasses.Bonuses.DefenceBonus>(), new List<Classes.DatabaseClasses.Bonuses.TestBonus>(), new List<string>()
                            , new List<Skill>(), RaceName, 0, 0, 0, 0, null, 0);
                        def = new CharacterDefinition(new List<Classes.DatabaseClasses.Bonuses.AttributeBonus>(), new List<Classes.DatabaseClasses.Bonuses.AbilityBonus>()
                            , new List<Classes.DatabaseClasses.Bonuses.DefenceBonus>(), new List<Classes.DatabaseClasses.Bonuses.TestBonus>()
                            , perks, attributes, skills, level, rank, Health, 0, magicClass, race, 0, 0, readedName);
                        xD.Add(new CharacterInstance(def, new List<Classes.DatabaseClasses.Items.ItemDefinition>(), new List<string>(), 21, 37, 476, "Tag"));
                        //Name = null;
                    }
                    else
                    {
                            Console.Write("21");
                        attributes.Add(new Attr(readerBase.GetString(2), readerBase.GetInt32(3)));
                    }

                }
#endregion
                
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.InnerException.Message);
            }
            Console.Write("XDDD");
        }
        public void insertIntoTitle(string titleName)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand("INSERT INTO Tytul (Nazwa) Values(@title)", connection);
                cmd.Parameters.AddRange(new OleDbParameter[]
                {
                    new OleDbParameter("@title", titleName)
                });
                cmd.ExecuteNonQuery();
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.InnerException.Message);
            }
        }
        public void insertIntoSize(string sizeDesc)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand("INSERT INTO Rozmiar (Opis) Values(@desc)", connection);
                cmd.Parameters.AddRange(new OleDbParameter[]
                {
                    new OleDbParameter("@desc", sizeDesc)
                });
                cmd.ExecuteNonQuery();
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.InnerException.Message);
            }
        }
        public void insertIntoEffect(string effectName)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand("INSERT INTO Efekt (Nazwa) Values(@name)", connection);
                cmd.Parameters.AddRange(new OleDbParameter[]
                {
                    new OleDbParameter("@name", effectName)
                });
                cmd.ExecuteNonQuery();
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.InnerException.Message);
            }
        }
        public void insertIntoTestBonus(int testValue)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand("INSERT INTO PremiaDoTestow (WartoscPremii) Values(@value)", connection);
                cmd.Parameters.AddRange(new OleDbParameter[]
                {
                    new OleDbParameter("@value", testValue)
                });
                cmd.ExecuteNonQuery();
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.InnerException.Message);
            }
        }
        public void insertIntoSkillBonus(int skillValue)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand("INSERT INTO PremiaDoUmiejetnosci (WartoscPremii) Values(@value)", connection);
                cmd.Parameters.AddRange(new OleDbParameter[]
                {
                    new OleDbParameter("@value", skillValue)
                });
                cmd.ExecuteNonQuery();
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.InnerException.Message);
            }
        }
        public void insertIntoAttributeBonus(int attributeValue)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand("INSERT INTO PremiaDoAtrybutu (WartoscPremii) Values(@value)", connection);
                cmd.Parameters.AddRange(new OleDbParameter[]
                {
                    new OleDbParameter("@value", attributeValue)
                });
                cmd.ExecuteNonQuery();
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.InnerException.Message);
            }
        }
    }
}
