﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes
{
    public static class Dice
    {
        private static Random rand;

        static Dice()
        {
            rand = new Random();
        }

        public static string DiceToString(int amount, int type)
        {
            return amount.ToString() + "d" + type.ToString();
        }
        public static int[] StringToDice(string s)
        {
            int[] res = new int[2];
            string t = null, a = null;
            int d = s.IndexOf('d');
            if (d < 0) d = s.IndexOf('k');
            if (d < 0) return null;

            for(int i=0; i<s.Length; i++)
            {
                if (i < d) t += s[i];
                if (i > d) a += s[i];
            }

            res[0] = Int32.Parse(t);
            res[1] = Int32.Parse(a);

            return res;
        }
        public static int Roll(int amount, int type)
        {
            return rand.Next(amount, amount * type);
        }
        public static int Roll(string DiceString)
        {
            int[] d = Dice.StringToDice(DiceString);

            return Roll(d[0], d[1]);
        }
    }
}
