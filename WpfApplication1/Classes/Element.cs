﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApplication1
{
    class Element
    {
        public delegate void positionDelegate();
        public delegate void sizeDelegate();

        public dynamic element { get; }
        public bool isActive = false;
        Dictionary<string, positionDelegate> position = new Dictionary<string, positionDelegate>();
        Dictionary<string, sizeDelegate> size = new Dictionary<string, sizeDelegate>();


        /// <param name="theElement">Element from WPF toolbox.</param>
        public Element(dynamic theElement)
        {
            element = theElement;
        }

        /// <summary>
        /// Function which add entry function to dictionary functions of POSITIONS.
        /// </summary>
        /// <param name="positionName">name of function what u want to add</param>
        /// <param name="func">function what u want to add</param>
        public void AddSize(string positionName, sizeDelegate func)
        {
            size.Add(positionName, func);
        }
        public void SetSize(string positionName)
        {
            size[positionName].DynamicInvoke();
        }

        /// <summary>
        /// Function which add entry function to dictionary functions of SIZE.
        /// </summary>
        /// <param name="positionName">name of function what u want to add</param>
        /// <param name="func">function what u want to add</param>
        public void AddPosition(string positionName, positionDelegate func)
        {
            position.Add(positionName, func);
        }
        public void SetPosioton(string positionName)
        {
            position[positionName].DynamicInvoke();
        }
    }
}
