﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Items
{
    class Enchantments
    {
        public int Level;
        public string Special;
        public KeyValuePair<string, string> Property;
        public int RequiredLevel;

        public string Description;
        public string Type;

        protected Enchantments() { }

        public Enchantments(
            int lv,
            string special,
            KeyValuePair<string, string> property,
            int requiredlv,
            string description,
            string type)
        {

        }

    }
}
