﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Items
{
    class ItemDefinition
    {
        public struct dmg
        {
            int dice;
            int val;
        };
        public struct cash
        {
            int copper;
            int silver;
            int gold;
            int platinum;
            int astralDiamond;
        };
        public string ItemType;

        #region Fields

        public string Name;
        public cash TradeValue;
        public float Weight;
        public int Amount;


        // for mount
        public int Capacity;
        // for armor and shields
        public KeyValuePair<string, string> WeightClass;
        public int ArmorResistBonus;
        public int SpeedPenalty;
        public int MinBonus;
        public int Bonus;
        // for weapon
        public KeyValuePair<string, string> Property;
        public KeyValuePair<string, string> WeaponGroup;
        public KeyValuePair<string, string> TypeOfRange;
        public int Range;
        public dmg Damage;
        public int ProficiencyBonus;

        #endregion

    }
}
