﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Characters
{
    class Class
    {
        #region Fields

        protected List<Bonuses.AttributeBonus> AttrBonus;
        protected List<Bonuses.AbilityBonus> AbilityBonus;
        protected List<Bonuses.DefenceBonus> DefBonus;
        protected List<Bonuses.TestBonus> TestBonus;
        public List<KeyValuePair<string, string>> ArmorProficiency;
        public List<KeyValuePair<string, string>> Ability;
        protected List<Skill> Skill;
        public string Name;


        #endregion

        public Class(List<Bonuses.AttributeBonus> attrbonuses,
            List<Bonuses.AbilityBonus> abilitybonuses,
            List<Bonuses.DefenceBonus> defbonuses,
            List<Bonuses.TestBonus> testbonuses,
            List<KeyValuePair<string, string>> armorproficiencies,
            List<KeyValuePair<string, string>> abilities,
            List<Skill> skills,
            string name)
        {
            AttrBonus = new List<Bonuses.AttributeBonus>(attrbonuses);
            AbilityBonus = new List<Bonuses.AbilityBonus>(abilitybonuses);
            DefBonus = new List<Bonuses.DefenceBonus>(defbonuses);
            TestBonus = new List<Bonuses.TestBonus>(testbonuses);
            ArmorProficiency = new List<KeyValuePair<string, string>>(armorproficiencies);
            Ability = new List<KeyValuePair<string, string>>(abilities);
            Skill = new List<Characters.Skill>(skills);
            Name = name;
        }
    }
}
