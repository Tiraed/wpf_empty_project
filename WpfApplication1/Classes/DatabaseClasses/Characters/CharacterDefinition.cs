﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Characters
{
    class CharacterDefinition
    {
        #region Fields

        public List<Bonuses.AttributeBonus> AttrBonus;
        public List<Bonuses.AbilityBonus> AbilityBonus;
        public List<Bonuses.DefenceBonus> DefBonus;
        public List<Bonuses.TestBonus> TestBonus;
        public List<Characters.Perk> Perk;
        public List<Attr> Attribute;
        public List<Skill> Skill;

        public int Level;
        public string Rank;
        public int MaxHP;
        public int MaxHealingSurges;
        public Class Class;
        public Race Race;
        public int Height;
        public int Weight;
        public string Name;

        #endregion

        #region Constructors
            protected CharacterDefinition() { }

            public CharacterDefinition(List<Bonuses.AttributeBonus> attrbonuses,
                List<Bonuses.AbilityBonus> abilitybonuses,
                List<Bonuses.DefenceBonus> defbonuses,
                List<Bonuses.TestBonus> testbonuses,
                List<Characters.Perk> perks,
                List<Attr> attributes,
                List<Skill> skills,
                int level,
                string rank,
                int maxhp,
                int maxhealingsurges,
                Class c,
                Race r,
                int heigth,
                int weight,
                string name)
        {
            AttrBonus = new List<Bonuses.AttributeBonus>(attrbonuses);
            AbilityBonus = new List<Bonuses.AbilityBonus>(abilitybonuses);
            DefBonus = new List<Bonuses.DefenceBonus>(defbonuses);
            TestBonus = new List<Bonuses.TestBonus>(testbonuses);
            Perk = new List<Characters.Perk>(perks);
            Attribute = new List<Attr>(attributes);
            Skill = new List<Characters.Skill>(skills);
            Level = level;
            Rank = rank;
            MaxHP = maxhp;
            MaxHealingSurges = maxhealingsurges;
            Class = c;
            Race = r;
            Height = heigth;
            Weight = weight;
            Name = name;
        }
        #endregion


        public int GetAttributeVal(string name)
        {
            for(int i=0; i<Attribute.Count; i++)
            {
                if (Attribute[i].Name == name) return Attribute[i].Value;
            }

            return -1;
        }
        public int GetAttrBonusVal(string name)
        {
            for (int i = 0; i < AttrBonus.Count; i++)
            {
                if (AttrBonus[i].Attribute == name) return AttrBonus[i].Value;
            }

            return -1;
        }
    }
}
