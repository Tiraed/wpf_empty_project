﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Characters
{
    public class Attr
    {
        public string Name;
        public int Value;
        
        public Attr(string name, int val)
        {
            Name = name;
            Value = val;
        }
    }
}
