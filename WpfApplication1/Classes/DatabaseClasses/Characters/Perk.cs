﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Characters
{
    class Perk
    {
        public string Name;
        public string Advanages;
        public string Requirements;

        protected Perk() { }

        public Perk( string name, string advantages, string requirements)
        {
            Name = name;
            Advanages = advantages;
            Requirements = requirements;
        }

    }
}
