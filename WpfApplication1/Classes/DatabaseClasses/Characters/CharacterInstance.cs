﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Characters
{
    class CharacterInstance : CharacterDefinition
    {
        #region Fields

        public List<Items.ItemDefinition> Item;
        public List<string> State;
        
        public int HP;
        public int HealingSurges;
        public int Initiavive;
        public string Descripion;


        #endregion


        public CharacterInstance(
            CharacterDefinition chardef, 
            List<Items.ItemDefinition> items, 
            List<string> state, 
            int hp, 
            int healingsurges, 
            int lv, 
            string description)
        {
            AttrBonus = new List<Bonuses.AttributeBonus>(chardef.AttrBonus);
            AbilityBonus = new List<Bonuses.AbilityBonus>(chardef.AbilityBonus);
            DefBonus = new List<Bonuses.DefenceBonus>(chardef.DefBonus);
            TestBonus = new List<Bonuses.TestBonus>(chardef.TestBonus);
            Perk = new List<Characters.Perk>(chardef.Perk);
            Attribute = new List<Attr>(chardef .Attribute);
            Skill = new List<Characters.Skill>(chardef.Skill);
            Level = chardef.Level;
            Rank = chardef.Rank;
            MaxHP = chardef.MaxHP;
            MaxHealingSurges = chardef.MaxHealingSurges;
            Class = chardef.Class;
            Race = chardef.Race;
            Height = chardef.Height;
            Weight = chardef.Weight;
            Name = chardef.Name;
            State = new List<string>(state);
            HP = hp;
            HealingSurges = healingsurges;
            Level = lv;
            Descripion = description;

        }

        public CharacterInstance(
            List<Bonuses.AttributeBonus> attrbonuses,
            List<Bonuses.AbilityBonus> abilitybonuses,
            List<Bonuses.DefenceBonus> defbonuses,
            List<Bonuses.TestBonus> testbonuses,
            List<Characters.Perk> perks,
            List<Attr> attributes,
            List<Skill> skills,
            string rank,
            int maxhp,
            int maxhealingsurges,
            Class c,
            Race r,
            int heigth,
            int weight,
            string name,
            List<Items.ItemDefinition> items,
            List<string> state,
            int hp,
            int healingsurges,
            int lv,
            string description)
        {
            AttrBonus = new List<Bonuses.AttributeBonus>(attrbonuses);
            AbilityBonus = new List<Bonuses.AbilityBonus>(abilitybonuses);
            DefBonus = new List<Bonuses.DefenceBonus>(defbonuses);
            TestBonus = new List<Bonuses.TestBonus>(testbonuses);
            Perk = new List<Characters.Perk>(perks);
            Attribute = new List<Attr>(attributes);
            Skill = new List<Characters.Skill>(skills);
            Level = lv;
            Rank = rank;
            MaxHP = maxhp;
            MaxHealingSurges = maxhealingsurges;
            Class = c;
            Race = r;
            Height = heigth;
            Weight = weight;
            Name = name;
            State = new List<string>(state);
            HP = hp;
            HealingSurges = healingsurges;
            Level = lv;
            Descripion = description;
        }

    }
}
