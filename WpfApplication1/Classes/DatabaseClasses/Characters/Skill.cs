﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Characters
{
    class Skill
    {
        public string ActionTime;
        public int RequiredLevel;
        public string Name;
        public string Description;
        public int ProficiencyBonus;
        public string RequiredClass;
        public List<string> KeyWords;

        protected Skill() { }

        public Skill(string actiontime, 
            int requiredlv,
            string name,
            string description,
            int proficiencybonus,
            string requiredclass,
            string[] keywords)
        {
            KeyWords = new List<string>();
            ActionTime = actiontime;
            RequiredLevel = requiredlv;
            Name = name;
            Description = description;
            ProficiencyBonus = proficiencybonus;
            RequiredClass = requiredclass;
            foreach(string k in keywords)
            {
                KeyWords.Add(k);
            }
        }

    }
}
