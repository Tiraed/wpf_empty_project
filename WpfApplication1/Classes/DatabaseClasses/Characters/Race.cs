﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses.Characters
{
    class Race
    {
        #region Fields

        protected List<Bonuses.AttributeBonus> AttrBonus;
        protected List<Bonuses.AbilityBonus> AbilityBonus;
        protected List<Bonuses.DefenceBonus> DefBonus;
        protected List<Bonuses.TestBonus> TestBonus;
        public List<string> CasualNames;
        public List<Skill> Skill;

        public string Name;
        public int MaxWeight;
        public int MinWeight;
        public int MaxHeight;
        public int MinHeight;
        public string Size;
        public int Speed;

        #endregion

        public Race(List<Bonuses.AttributeBonus> attrbonuses,
            List<Bonuses.AbilityBonus> abilitybonuses,
            List<Bonuses.DefenceBonus> defbonuses,
            List<Bonuses.TestBonus> testbonuses,
            List<string> casualnames,
            List<Skill> skills,
            string name,
            int maxweight,
            int minweight,
            int maxheiht,
            int minheight,
            string size,
            int speed)
        {
            AttrBonus = new List<Bonuses.AttributeBonus>(attrbonuses);
            AbilityBonus = new List<Bonuses.AbilityBonus>(abilitybonuses);
            DefBonus = new List<Bonuses.DefenceBonus>(defbonuses);
            TestBonus = new List<Bonuses.TestBonus>(testbonuses);
            CasualNames = new List<string>(casualnames);
            Skill = new List<Skill>(skills);
            Name = name;
            MaxWeight = maxweight;
            MinWeight = minweight;
            MaxHeight = maxheiht;
            MinHeight = minheight;
            Size = size;
            Speed = speed;
        }

    }
}