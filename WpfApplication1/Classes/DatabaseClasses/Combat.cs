﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes.DatabaseClasses
{
    class Combat
    {
        public List<Characters.CharacterInstance> Characters;
        public List<List<Characters.CharacterInstance>> Groups;
        public List<string> GroupNames = new List<string>();

        public Combat()
        {
            Characters = new List<DatabaseClasses.Characters.CharacterInstance>();
            Groups = new List<List<DatabaseClasses.Characters.CharacterInstance>>();
        }
        public Combat(Combat c)
        {
            Characters = new List<DatabaseClasses.Characters.CharacterInstance>(c.Characters);
            Groups = new List<List<Characters.CharacterInstance>>(c.Groups);
        }
        
        public void AddCharacter(Characters.CharacterInstance c)
        {
            Characters.Add(c);
        }
        public void AddCharacter(Characters.CharacterInstance c, string group)
        {
            Characters.Add(c);

            for(int i=0; i<GroupNames.Count; i++)
            {
                if(group == GroupNames[i])
                {
                    Groups[i].Add(c);
                    break;
                }
                if(i+1 == GroupNames.Count)
                {
                    GroupNames.Add(group);
                    Groups[i+1].Add(c);
                }
            }

         }
        public void AddCharacters(Characters.CharacterInstance[] characters)
        {
            foreach (Characters.CharacterInstance c in characters)
            {
                AddCharacter(c);
            }
        }
        public void AddCharacters(Characters.CharacterInstance[] characters, string group)
        {
            foreach (Characters.CharacterInstance c in characters)
            {
                AddCharacter(c, group);
            }
        }
        public void RemoveCharacter(Characters.CharacterInstance c)
        {
            for (int i = 0; i < Characters.Count; i++)
            {
                if (Characters[i] == c)
                {
                    Characters.RemoveAt(i);
                    break;
                }
            }
            for(int g=0; g<Groups.Count; g++)
            {
                RemoveFromGroup(c, GroupNames[g]);
            }
        }
        public void RemoveCharacters(Characters.CharacterInstance[] characters)
        {
            foreach(Characters.CharacterInstance c in characters)
            {
                RemoveCharacter(c);
            }
        }
        public void RemoveFromGroup(Characters.CharacterInstance c, string group)
        {
            for(int g=0; g<Groups.Count; g++)
            {
                if(group == GroupNames[g])
                {
                    for (int i = 0; i < Groups[g].Count; i++)
                    {
                        if(Groups[g][i] == c)
                        {
                            Groups[g].RemoveAt(i);
                            break;
                        }
                    }
                    break;
                }

            }
        }
        public void AddGroup(string name)
        {
            Groups.Add(new List<Characters.CharacterInstance>());
            GroupNames.Add(name);
        }
        public void RemoveGroup(string name)
        {
            for(int i=0; i<Groups.Count; i++)
            {
                if(GroupNames[i] == name)
                {
                    Groups.RemoveAt(i);
                    GroupNames.RemoveAt(i);
                    break;
                }
            }
        }
        public void EditGroupName(string name, string newname)
        {
            for(int i=0; i<GroupNames.Count; i++)
            {
                if(GroupNames[i] == name)
                {
                    GroupNames[i] = newname;
                }
            }
        }
        public List<Characters.CharacterInstance> InitiativeTest()
        {
            List<Characters.CharacterInstance> cs = new List<DatabaseClasses.Characters.CharacterInstance>(Characters);

            // find initiative
            foreach (Characters.CharacterInstance c in cs)
            {
                c.Initiavive = c.GetAttributeVal("Zrecznosc");
                c.Initiavive += c.Initiavive / 2 - 5;
                c.Initiavive += c.GetAttrBonusVal("Zrecznosc");
                c.Initiavive += Dice.Roll("1d20");
            }

            cs.Sort(CompareByInitiative);

            return cs;
        }
            private static int CompareByInitiative(Characters.CharacterInstance x, Characters.CharacterInstance y)
            {
                if (x.Initiavive == 0)
                {
                    if (y.Initiavive == 0)
                    {
                        // If x is null and y is null, they're
                        // equal. 
                        return 0;
                    }
                    else
                    {
                        // If x is null and y is not null, y
                        // is greater. 
                        return -1;
                    }
                }
                else
                {
                    // If x is not null...
                    //
                    if (y.Initiavive == 0)
                    // ...and y is null, x is greater.
                    {
                        return 1;
                    }
                    else
                    {
                        return x.Initiavive.CompareTo(y.Initiavive);
                    }
                }
            }

    }
}
