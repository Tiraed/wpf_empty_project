﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Classes
{
    class World
    {
        public string Name;
        public List<DatabaseClasses.Combat> Combats;

        public World(string name)
        {
            Name = name;
            Combats = new List<DatabaseClasses.Combat>();
        }
        public void EditName(string newname)
        {
            Name = newname;
        }
    }
}
