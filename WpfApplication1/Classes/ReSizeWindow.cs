﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows;

namespace WpfApplication1
{
    class ReSizeWindow
    {
        private static double _actualScale, _actualWidthScale, _actualHeigthScale;
        public static double actualScale { get { return _actualScale; } }
        public static double actualWidthScale { get { return _actualWidthScale; } }
        public static double actualHeigthScale { get { return _actualHeigthScale; } }
        
        private static double initWidth, initHeigth;

       

        /// <summary>
        /// Function to initialize start Width and Heigth values
        /// </summary>
        /// <param name="width"></param>
        /// <param name="heigth"></param>
        public static void InitReScale(double width, double heigth)
        {
            initWidth = width;
            initHeigth = heigth;

            ReScale(width, heigth);
        }

        /// <summary>
        /// Function to calculate actual scale from orginal
        /// </summary>
        /// <param name="width"></param>
        /// <param name="heigth"></param>
        public static void ReScale(double width, double heigth)
        {
            _actualWidthScale = width / initWidth;
            _actualHeigthScale = heigth / initHeigth;

            _actualScale = (_actualWidthScale + _actualHeigthScale) / 2;
        }


        /// <summary>
        /// Function to scale elements and they position on MainWindows.xaml 
        ///     If the value is lower than 0 it doesn't change.
        /// </summary>
        /// <param name="elementToScale">The element what we need to scale and change position(Button, TextBox, etc.)</param>
        /// <param name="scaleX">Scale in X axle</param>
        /// <param name="scaleY">Scale in Y axle</param>
        /// <param name="bottom">new Margain.Bottom</param>
        /// <param name="left">new Margain.Left</param>
        /// <param name="right">new Margain.Right</param>
        /// <param name="top">new Margain.Top</param>
        public static void LetsScaleElement(dynamic elementToScale, double scaleX, double scaleY,
                                            double bottom, double left, double right, double top)
        {
            Thickness m = elementToScale.Margin;

            if (bottom > 0) m.Bottom = bottom;
            if (left > 0) m.Left = left;
            if (right > 0) m.Right = right;
            if (top > 0) m.Top = top;
            
            elementToScale.Margin = m;

            LetsScaleElement(elementToScale, scaleX, scaleY);
        }

        /// <summary>
        /// Function to scale elements on MainWindows.xaml
        /// </summary>
        /// <param name="elementToScale">The element what we need to scale (Button, ListBox, etc.)</param>
        /// <param name="scaleX">Scale in X axle</param>
        /// <param name="scaleY">Scale in y axle</param>
        public static void LetsScaleElement(dynamic elementToScale, double scaleX, double scaleY)
        {
            TransformGroup tg = elementToScale.RenderTransform as TransformGroup;

            if (tg != null)
            {
                ScaleTransform st = tg.Children[0] as ScaleTransform;
                st.ScaleX = scaleX;
                st.ScaleY = scaleY;
            }
        }

        /// <summary>
        /// Function to change margin. NOTICE: Any value which is smaller than 0, won't change.
        /// </summary>
        /// <param name="elementToScale">The element what we need to change margin (Button, ListBox, etc.)</param>
        /// <param name="bottom">new Margain.Bottom</param>
        /// <param name="left">new Margain.Left</param>
        /// <param name="right">new Margain.Right</param>
        /// <param name="top">new Margain.Top</param>
        public static void changeMargin(dynamic elementToScale, double bottom, double left, double right, double top)
        {
            Thickness m = elementToScale.Margin;

            if (bottom > 0) m.Bottom = bottom;
            if (left > 0) m.Left = left;
            if (right > 0) m.Right = right;
            if (top > 0) m.Top = top;

            elementToScale.Margin = m;
        }


    }
}
