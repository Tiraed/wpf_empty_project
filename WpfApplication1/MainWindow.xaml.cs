﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using WpfApplication1.Classes.DatabaseClasses;
using WpfApplication1.Classes.DatabaseClasses.Characters;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double heigth;
        double width;


        Element eSwiatButton;
        Element eSwiatListBox;
        Element eSwiatGridBody;

        Element eSpotkanieBitewneButton;
        Element eSpotkanieBitewneListBox;
        Element eSpotkanieBitewneGridBody;
        void eSwiatListBox_full() { SwiatListBox.Height = (heigth - 2 * SwiatButton.Height + 1) - 30; }
        void eSwiatListBox_hide() { SwiatListBox.Height = 0; }

        void eSwiatGridBody_full()
        {
            SwiatGridBody.Height = heigth - 180;
            SwiatGridBody.Width = width - SwiatListBox.Width - 45;
        }

        void eSwiatGridBody_hide()
        {
            SwiatGridBody.Height = 0;
            SwiatGridBody.Width = 0;
        }

        void eSpotkanieBitewneGridBody_full()
        {
            SpotkanieBitewneGridBody.Height = heigth - 180;
            SpotkanieBitewneGridBody.Width = width - SpotkanieBitewneListBox.Width - 45;
        }

        void eSpotkanieBitewneGridBody_hide()
        {
            SpotkanieBitewneGridBody.Height = 1;
            SpotkanieBitewneGridBody.Width = 1;
        }

        void eSpotkanieBitewneButton_full() { ReSizeWindow.changeMargin(SpotkanieBitewneButton, -1, -1, -1, SwiatButton.Height + 15); }
        void eSpotkanieBitewneButton_hide() { ReSizeWindow.changeMargin(SpotkanieBitewneButton, -1, -1, -1, heigth - SpotkanieBitewneButton.Height - 16); }
        void eSpotkanieBitewneListBox_full() { SpotkanieBitewneListBox.Height = (heigth - SpotkanieBitewneButton.Height * 2 + 1) - 30; }
        void eSpotkanieBitewneListBox_hide() { SpotkanieBitewneListBox.Height = 0; }
        void eSpotkanieBitewneListBox_margin() { ReSizeWindow.changeMargin(SpotkanieBitewneListBox, -1, -1, -1, 2 * SpotkanieBitewneButton.Height + 15); }


        List<CharacterInstance> WAPIESZ;

        //public 
        public MainWindow()
        {
            InitializeComponent();
            DBGetter abc = new DBGetter();
            abc.selectAll();
            Combat wombat = new Combat();
            for (int i = 0; i < 5; i++)
            {
                wombat.AddCharacter(abc.xD.ElementAt(0), "9/11 WAS INSIDE JOB");
            }
            WAPIESZ = new List<CharacterInstance>(wombat.InitiativeTest());
            WAPIESZ = wombat.InitiativeTest();

            #region Elements setting

            #region Element Swiat

            eSwiatButton = new Element(SwiatButton);
                    eSwiatButton.isActive = true;

                    eSwiatListBox = new Element(SwiatListBox);
                    
                    eSwiatListBox.AddSize("full", eSwiatListBox_full);
                   
                    eSwiatListBox.AddSize("hide", eSwiatListBox_hide);


            eSwiatGridBody = new Element(SwiatGridBody);
                eSwiatGridBody.isActive = true;
                
                eSwiatGridBody.AddSize("full", eSwiatGridBody_full);

            eSwiatGridBody.AddSize("hide", eSwiatGridBody_hide);

            eSpotkanieBitewneGridBody = new Element(SpotkanieBitewneGridBody);
            eSpotkanieBitewneGridBody.isActive = false;

            eSpotkanieBitewneGridBody.AddSize("full", eSpotkanieBitewneGridBody_full);

            eSpotkanieBitewneGridBody.AddSize("hide", eSpotkanieBitewneGridBody_hide);



            #endregion //Element Swiat

            #region Spotkanie Bitewne

            eSpotkanieBitewneButton = new Element(SpotkanieBitewneButton);
                    eSpotkanieBitewneButton.isActive = false;
                        
                        eSpotkanieBitewneButton.AddPosition("full", eSpotkanieBitewneButton_full);
                       eSpotkanieBitewneButton.AddPosition("hide", eSpotkanieBitewneButton_hide);



           eSpotkanieBitewneListBox = new Element(SpotkanieBitewneListBox);
                    eSpotkanieBitewneListBox.AddSize("full", eSpotkanieBitewneListBox_full);
                    eSpotkanieBitewneListBox.AddSize("hide", eSpotkanieBitewneListBox_hide);

                   eSpotkanieBitewneListBox.AddPosition("margin", eSpotkanieBitewneListBox_margin);

                #endregion //Spotkanie Bitewne

            #endregion //Elements declaration

        }

        private void PrintData()
        {
            PrintBox.Clear();

            foreach (CharacterInstance iksDe in WAPIESZ)
            {
                PrintBox.Text += iksDe.Name + " | ";

                foreach (var at in iksDe.Attribute)
                {
                    PrintBox.Text += " "+ at.Name + " " + at.Value;
                }
                PrintBox.Text += " | ";

                PrintBox.Text += "Level= " + iksDe.Level + " | ";

                PrintBox.Text += "HP= " + iksDe.HP + " | ";

                PrintBox.Text += "maxHP= " + iksDe.MaxHP + " | ";

                PrintBox.Text += "HealingSurges= " + iksDe.HealingSurges + " | ";

                PrintBox.Text += "Inicjative= " + iksDe.Initiavive + " | ";

                PrintBox.Text += "Inicjative= " + iksDe.Initiavive + " | ";

                PrintBox.Text += iksDe.Rank + " | ";

                foreach (var per in iksDe.Perk)
                {
                    PrintBox.Text += " " + per.Name;
                }
                PrintBox.Text += " | ";

                foreach (var sk in iksDe.Skill)
                {
                    PrintBox.Text += " " + sk.Name;
                }
                PrintBox.Text += " | ";

                PrintBox.Text +=  iksDe.Descripion + " | ";

                PrintBox.AppendText(Environment.NewLine);
            }


            SortedDictionary<int, string> kolejnosc = new SortedDictionary<int, string>();

            foreach (var item in WAPIESZ)
            {
                kolejnosc.Add(item.Initiavive, item.Name);
            }




            SpotkaniePrintBox.Clear();

            while (kolejnosc.Count > 0)
            {
                SpotkaniePrintBox.Text += kolejnosc.Last().Key + " " + kolejnosc.Last().Value;

                SpotkaniePrintBox.AppendText(Environment.NewLine);
                kolejnosc.Remove(kolejnosc.Last().Key);
            }

            
            kolejnosc = null;



        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            
            heigth = ((Panel)Application.Current.MainWindow.Content).ActualHeight;
            width = ((Panel)Application.Current.MainWindow.Content).ActualWidth;

            PrintData();

            try
            {
                

                if (ReSizeWindow.actualWidthScale > 0 || ReSizeWindow.actualHeigthScale > 0)
                {
                    ReSizeWindow.ReScale(width, heigth);

                    #region ReScale elements

                        if (eSwiatButton.isActive == true)
                        {
                            eSwiatListBox.SetSize("full");
                            eSwiatGridBody.SetSize("full");
                            eSpotkanieBitewneGridBody.SetSize("hile");
                        }
                        else
                        {
                            eSwiatListBox.SetSize("hide");
                            eSwiatGridBody.SetSize("hide");
                            eSpotkanieBitewneGridBody.SetSize("full");
                    }

                        if (eSpotkanieBitewneButton.isActive == true)
                        {
                            eSpotkanieBitewneListBox.SetSize("full");
                            eSpotkanieBitewneListBox.SetPosioton("margin");

                            eSpotkanieBitewneButton.SetPosioton("full");
                        }
                        else
                        {
                            eSpotkanieBitewneListBox.SetSize("hide");
                            eSpotkanieBitewneButton.SetPosioton("hide");
                        }

                    
                    #endregion //ReScale elements


                    TextBoxConsole.Text = "Done#" + "\n GH=" + SwiatGridBody.Height + "   heigth= " + heigth 
                                            + "\n Margin.Right=" + SwiatGridBody.Width + "\nMargin.Top= " + SwiatButton.Margin.Top
                                            + "\n Margin.Left=" + SwiatButton.Margin.Left+ "\nMargin.Bottom= " + SwiatButton.Margin.Bottom
                                            + "\n actualHeigthScale=" + ReSizeWindow.actualHeigthScale;
                }
                else ReSizeWindow.InitReScale(width, heigth);
            }
            catch (Exception sizeException)
            {

                TextBoxConsole.Text += sizeException.Message + "\n";
            }
        }


        private void SwiatButton_Click(object sender, RoutedEventArgs e)
        {
            eSwiatButton.isActive = true;
            eSpotkanieBitewneButton.isActive = false;

            eSpotkanieBitewneGridBody.SetSize("hide");
            eSwiatGridBody.SetSize("full");
            eSpotkanieBitewneListBox.SetSize("hide");
            eSwiatListBox.SetSize("full");
            PrintData();
            eSpotkanieBitewneButton.SetPosioton("hide");

        }

        private void SpotkanieBitewneButton_Click(object sender, RoutedEventArgs e)
        {
            eSpotkanieBitewneGridBody.SetSize("full");
            eSwiatButton.isActive = false;
            eSpotkanieBitewneButton.isActive = true;
            PrintData();

            eSpotkanieBitewneListBox.SetSize("full");
            eSwiatListBox.SetSize("hide");
            eSwiatGridBody.SetSize("hide");

            eSpotkanieBitewneListBox.SetPosioton("margin");

            eSpotkanieBitewneButton.SetPosioton("full");

        }

    }
}
